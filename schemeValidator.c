#include "schemeValidator.h"

int schemeValidator(char scheme[]) {
  int result = -1;
  if (isAlpha(scheme[0])) {
    result = 0;  
  }else if(isAlphaUpper(scheme[0])){
    result = 1;
  } else {
    return result;
  }   
  int i=1;
    while (scheme[i] != '\0') {
       if( isSpecial(scheme[i]) 
		|| isDigit(scheme[i])
		|| isAlpha(scheme[i])
	){
	 // Good, do nothing 
	} else if( isAlphaUpper(scheme[i])){
	  // UC Found
	  result = 1;
	}  else {
	  result = -1;
	  return result;
	}
	
      i++;
    } 
  return result;
}

bool isAlphaUpper(char c){
  return 'A' <= c && c <= 'Z';
}

bool isAlpha(char c) {
  return 'a' <= c && c <= 'z';
}

bool isSpecial(char c) {
  bool result = false;
  if (c == '+') result = true;
  else if (c == '-') result = true;
  else if (c == '.') result = true;
  return result;
}

bool isDigit(char c) {
  return '0' <= c && c <= '9';
}


